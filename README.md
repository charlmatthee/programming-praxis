# Programming Praxis Puzzle Answers

This repo contains my solutions to the puzzles presented at [[http://programmingpraxis.com|Programming Praxis]].

I am using these puzzles as a way to explore new languages as well as the idiomatic way in which to reason and solve problems in them.

Solutions will most likely be in:

* [[http://www.ruby-lang.org/en|ruby]] or [[http://jruby.org|jruby]]
* [[http://clojure.org|clojure]]
* [[http://www.haskell.org/haskellwiki/Haskell|haskell]]
